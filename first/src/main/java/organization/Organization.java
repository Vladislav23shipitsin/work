package organization;

import java.io.Serializable;
import java.util.Date;
import java.util.Collection;

public class Organization implements Serializable {

    private int id;
    private String code;
    private String name_full;
    private String name_short;
    private String inn;
    private CompanyType company_type;
    private String ogrn;
    private Date egrul_date;
    private Country country;
    private String fio_head;
    private String address;
    private String phone;
    private String e_mail;
    private String www;
    private boolean is_resident;
    private Collection<Securities> securities;

    public Organization(int id, String code, String name_full, String name_short, String inn, CompanyType company_type, String ogrn, Date egrul_date, Country country, String fio_head, String address, String phone, String e_mail, String www, boolean is_resident, Collection<Securities> securities) {
        this.id = id;
        this.code = code;
        this.name_full = name_full;
        this.name_short = name_short;
        this.inn = inn;
        this.company_type = company_type;
        this.ogrn = ogrn;
        this.egrul_date = egrul_date;
        this.country = country;
        this.fio_head = fio_head;
        this.address = address;
        this.phone = phone;
        this.e_mail = e_mail;
        this.www = www;
        this.is_resident = is_resident;
        this.securities = securities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName_full() {
        return name_full;
    }

    public void setName_full(String name_full) {
        this.name_full = name_full;
    }

    public String getName_short() {
        return name_short;
    }

    public void setName_short(String name_short) {
        this.name_short = name_short;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public CompanyType getCompany_type() {
        return company_type;
    }

    public void setCompany_type(CompanyType company_type) {
        this.company_type = company_type;
    }

    public String getOgrn() {
        return ogrn;
    }

    public void setOgrn(String ogrn) {
        this.ogrn = ogrn;
    }

    public Date getEgrul_date() {
        return egrul_date;
    }

    public void setEgrul_date(Date egrul_date) {
        this.egrul_date = egrul_date;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getFio_head() {
        return fio_head;
    }

    public void setFio_head(String fio_head) {
        this.fio_head = fio_head;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public boolean isIs_resident() {
        return is_resident;
    }

    public void setIs_resident(boolean is_resident) {
        this.is_resident = is_resident;
    }

    public Collection<Securities> getSecurities() {
        return securities;
    }

    public void setSecurities(Collection<Securities> securities) {
        this.securities = securities;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Organization{");
        sb.append("id=").append(id);
        sb.append(", code='").append(code).append('\'');
        sb.append(", name_full='").append(name_full).append('\'');
        sb.append(", name_short='").append(name_short).append('\'');
        sb.append(", inn='").append(inn).append('\'');
        sb.append(", company_type=").append(company_type);
        sb.append(", ogrn='").append(ogrn).append('\'');
        sb.append(", egrul_date='").append(egrul_date).append('\'');
        sb.append(", country=").append(country);
        sb.append(", fio_head='").append(fio_head).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", e_mail='").append(e_mail).append('\'');
        sb.append(", www='").append(www).append('\'');
        sb.append(", is_resident=").append(is_resident);
        sb.append(", securities=").append(securities);
        sb.append('}');
        return sb.toString();
    }
}
