package organization;

import java.util.Date;

public class Securities {
    private int id;
    private String code;
    private String name_full;
    private String cfi;
    private Date date_to;
    private Date state_reg_date;
    private State state;
    private Currency currency;

    public Securities(int id, String code, String name_full, String cfi, Date date_to, Date state_reg_date, State state, Currency currency) {
        this.id = id;
        this.code = code;
        this.name_full = name_full;
        this.cfi = cfi;
        this.date_to = date_to;
        this.state_reg_date = state_reg_date;
        this.state = state;
        this.currency = currency;
    }

    public boolean isExpired(){
        return date_to.compareTo(state_reg_date) > 0;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName_full() {
        return name_full;
    }

    public void setName_full(String name_full) {
        this.name_full = name_full;
    }

    public String getCfi() {
        return cfi;
    }

    public void setCfi(String cfi) {
        this.cfi = cfi;
    }

    public Date getDate_to() {
        return date_to;
    }

    public void setDate_to(Date date_to) {
        this.date_to = date_to;
    }

    public Date getState_reg_date() {
        return state_reg_date;
    }

    public void setState_reg_date(Date state_reg_date) {
        this.state_reg_date = state_reg_date;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Securities{");
        sb.append("id=").append(id);
        sb.append(", code='").append(code).append('\'');
        sb.append(", name_full='").append(name_full).append('\'');
        sb.append(", cfi='").append(cfi).append('\'');
        sb.append(", date_to='").append(date_to).append('\'');
        sb.append(", state_reg_date='").append(state_reg_date).append('\'');
        sb.append(", state=").append(state);
        sb.append(", currency=").append(currency);
        sb.append('}');
        return sb.toString();
    }
}
