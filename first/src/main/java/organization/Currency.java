package organization;

public class Currency {

    private int id;
    private String code;
    private String name_short;
    private String name_full;

    public Currency(int id, String code, String name_short, String name_full) {
        this.id = id;
        this.code = code;
        this.name_short = name_short;
        this.name_full = name_full;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName_short() {
        return name_short;
    }

    public void setName_short(String name_short) {
        this.name_short = name_short;
    }

    public String getName_full() {
        return name_full;
    }

    public void setName_full(String name_full) {
        this.name_full = name_full;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Currency{");
        sb.append("id=").append(id);
        sb.append(", code='").append(code).append('\'');
        sb.append(", name_short='").append(name_short).append('\'');
        sb.append(", name_full='").append(name_full).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
