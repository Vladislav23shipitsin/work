package can;

import organization.Organization;
import organization.Securities;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;


public class Work {

    public static Collection<Organization> getOrganizations(File fileGson){
        return JsonReader.getOrganizations(fileGson.toString());
    }

    public static void showOrganizations(Collection<Organization> organizations){
        final StringBuffer sb = new StringBuffer();

        organizations.forEach(org -> {
            sb.append(org.getName_short());
            sb.append(" - ");
            sb.append(showDate(org.getEgrul_date()));
            sb.append("\n");}
        );
        System.out.println(sb.toString());
    }

    public static void showSecurities(Collection<Organization> organizations){
        AtomicLong count = new AtomicLong();
        StringBuffer sb = new StringBuffer();
        organizations.forEach(org -> {
            org.getSecurities().stream().filter(Securities::isExpired).forEach(s -> {
                count.getAndIncrement();
                sb.append(s.getCode());
                sb.append(" - ");
                sb.append(showDate(s.getState_reg_date()));
                sb.append(" - ");
                sb.append(org.getName_full());
                sb.append("\n");
            });
        });
        sb.append("Всего : ");
        sb.append(count.get());
        System.out.println(sb.toString());
    }

    public static void showDate(Collection<Organization> organizations, Date date){
        StringBuffer sb = new StringBuffer();
        if(isCorrectDate(date)){
            organizations.stream().filter(o -> o.getEgrul_date().compareTo(date) > 0).forEach(o ->{
                sb.append(o.getName_short());
                sb.append(" - ");
                sb.append(showDate(o.getEgrul_date()));
                sb.append("\n");
            });
        }
        System.out.println(sb.toString());
    }

    public static void showCurrency(Collection<Organization> organizations, String codeCurrency){
        StringBuffer sb = new StringBuffer();
        organizations.forEach(org -> {
            org.getSecurities().stream().
                    filter( s -> s.getCurrency().getCode().equals(codeCurrency)).
                    forEach(s -> {
                sb.append(s.getId());
                sb.append(" - ");
                sb.append(s.getCode());
                sb.append("\n");
            });
        });
        System.out.println(sb.toString());
    }

    public static boolean isCorrectDate(Date date){
        return true;
    }

    public static String showDate(Date date){
        StringBuilder sb = new StringBuilder();
        sb.append(date.getYear() + 1900);
        sb.append("-");
        sb.append(date.getMonth());
        sb.append("-");
        sb.append(date.getDay());
        return sb.toString();
    }

}
