package can;

import com.google.gson.Gson;
import organization.Organization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Main {

    private static final String fileName  = "C:\\JavaProjects\\work\\test.json";

    public static Collection<Organization> getOrgans(){
        StringBuilder sb = new StringBuilder();
        try {
            Scanner inputScanner = new Scanner(new File(fileName));
            while(inputScanner.hasNext()){
                sb.append(inputScanner.next());
            }
            inputScanner.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return JsonReader.getOrganizations(sb.toString());
    }


    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       Collection<Organization> organizations = getOrgans();

       System.out.println("Все имеющиеся компании : ");
       Work.showOrganizations(organizations);
       System.out.println("Все ценные бумаги, которые просрочены на текущий день");
       Work.showSecurities(organizations);

       System.out.println();
       System.out.print("Введите дату в формате ГГГГ-ММ-ДД :");
       String dateStr = scanner.next();

       String[] strings = dateStr.split("-");
       Date date = new Date(Integer.valueOf(strings[0]) - 1900,Integer.valueOf(strings[1]),Integer.valueOf(strings[2]));
       System.out.println("Организации основанные после указанной даты: ");
       Work.showDate(organizations,date);

       System.out.print("Введите код валюты: ");
       String code = scanner.next();
       System.out.println("Все ценные бумаги использующие этот код валюты: ");
       Work.showCurrency(organizations,code);

    }
}
