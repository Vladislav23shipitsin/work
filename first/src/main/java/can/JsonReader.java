package can;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import organization.Organization;

import java.util.Collection;

public class JsonReader {
    private static final Gson GSON = new Gson();

    public static Organization getOrganization(String gsonString){
        return GSON.fromJson(gsonString,Organization.class);
    }

    public static Collection<Organization> getOrganizations(String gsonString){
        return GSON.fromJson(gsonString,new TypeToken<Collection<Organization>>(){}.getType());
    }



}
