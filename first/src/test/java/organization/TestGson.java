package organization;

import can.JsonReader;
import com.google.gson.Gson;
import org.junit.Test;

import java.util.Collection;

public class TestGson {

    private static final Gson GSON = new Gson();

    @Test
    public void test(){
        Collection<Organization> organizations = JsonReader.getOrganizations(s);

        System.out.println(organizations.size());
        for(Organization o : organizations){
            System.out.println(o);
        }
    }

    String s = "[\n" +
            "{\n" +
            "    \"id\": 1,\n" +
            "    \"code\": \"IRNP\",\n" +
            "    \"name_full\": \"Акционерное общество \\\"Иркутскпродукт\\\"\",\n" +
            "    \"name_short\": \"АО \\\"Иркутскпродукт\\\"\",\n" +
            "    \"inn\": \"38001230798\",\n" +
            "    \"company_type\": {\n" +
            "        \"id\": 16,\n" +
            "        \"name_short\": \"АО\",\n" +
            "        \"name_full\": \"Акционерное общество\"\n" +
            "    },\n" +
            "    \"ogrn\": \"1323800208080\",\n" +
            "    \"egrul_date\": \"2000-09-05\",\n" +
            "    \"country\": {\n" +
            "        \"id\": 190,\n" +
            "        \"code\": \"RU\",\n" +
            "        \"name\": \"РОССИЯ\"\n" +
            "    },\n" +
            "    \"fio_head\": \"Семенов Василий Игнатьевич\",\n" +
            "    \"address\": \"664901, г. Иркутск, ул. Ленина, д.15\",\n" +
            "    \"phone\": \"(3952) 32-12-11\",\n" +
            "    \"e_mail\": \"inp@knp.ros.ru\",\n" +
            "    \"www\": \"http:\\/\\/www.inpk.ru\\/\",\n" +
            "    \"is_resident\": true,\n" +
            "    \"securities\": [\n" +
            "        {\n" +
            "            \"id\": 10000801,\n" +
            "            \"code\": \"RU0005702712\",\n" +
            "            \"name_full\": \"Акции привилегированные типа А АО \\\"Иркутскпродукт\\\"\",\n" +
            "            \"cfi\": \"ETXXAR\",\n" +
            "            \"date_to\": \"2015-10-19\",\n" +
            "            \"state_reg_date\": \"2001-12-04\",\n" +
            "            \"state\": {\n" +
            "                \"id\": 2,\n" +
            "                \"name\": \"Аннулирован\"\n" +
            "            },                \n" +
            "\t\t\t\"currency\": {\n" +
            "                    \"id\": 6,\n" +
            "                    \"code\": \"RUB\",\n" +
            "                    \"name_short\": \"руб.\",\n" +
            "                    \"name_full\": \"Рубли\"\n" +
            "                }\n" +
            "\t\t},\n" +
            "\t\t{\n" +
            "            \"id\": 10000101,\n" +
            "            \"code\": \"RU0005118323\",\n" +
            "            \"name_full\": \"Облигации  \\\"Иркутскпродукт\\\"\",\n" +
            "            \"cfi\": \"EZXXAR\",\n" +
            "            \"date_to\": \"2020-10-23\",\n" +
            "            \"state_reg_date\": \"2002-11-08\",\n" +
            "            \"state\": {\n" +
            "                \"id\": 1,\n" +
            "                \"name\": \"Действующий\"\n" +
            "            },                \n" +
            "\t\t\t\"currency\": {\n" +
            "                    \"id\": 1,\n" +
            "                    \"code\": \"USD\",\n" +
            "                    \"name_short\": \"дол.\",\n" +
            "                    \"name_full\": \"Доллары\"\n" +
            "            }\n" +
            "\t\t}\n" +
            "\t\t\n" +
            "\t\t\t\t\t]\n" +
            "}\n" +
            "]";
}
